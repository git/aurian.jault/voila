import { Component } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';
import { FooterComponent } from '../footer/footer.component';
import { ProjetService } from '../../services/projet';
import { Projet } from '../../models/projet';
import { NgFor } from '@angular/common';
import { Diplome } from '../../models/diplomes';

@Component({
  selector: 'app-main-menu',
  standalone: true,
  imports: [NavbarComponent, FooterComponent, NgFor],
  providers: [],
  templateUrl: './main-menu.component.html',
  styleUrl: './main-menu.component.css'
})
export class MainMenuComponent {
  public projs: Projet[];
  public dipls: Diplome[];

  constructor(public service: ProjetService)
  {
    this.projs = service.getProjets();
    this.dipls = service.getDiplomes();
  }
}
