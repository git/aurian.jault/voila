import { Injectable } from "@angular/core";
import { Projet } from "../models/projet";
import { Diplome } from "../models/diplomes";
import { PROJ } from "../datas/projet.stub";
import { DIPLO } from "../datas/projet.stub";

@Injectable({
  providedIn: 'root'
})
export class ProjetService {
  private projs: Projet[] = PROJ;
  private dipls : Diplome[] = DIPLO;

  constructor() {}

  getProjets(): Projet[]
  {
    return this.projs;
  }

  getDiplomes(): Diplome[]
  {
    return this.dipls;
  }
}
