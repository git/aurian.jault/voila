export interface Projet {
  description: string;
  img: string;
  name: string;
  link: string;
}
