import { Projet } from "../models/projet"
import { Diplome } from "../models/diplomes"

export const PROJ : Projet[]  = [
  {name: "Archi-VR", img: "assets/pics/map.png", description: "Application de génération de visites virtuelles. Cette application à était réalisé entièrement en PHP et Javascript durant ma 3ème année de BUT informatique. Nous étions 3 pour réaliser de bout en bout ce projet.", link: "https://github.com/AurianJault/archivr"},
  {name: "PassWorld", img: "assets/pics/nft.png", description: "Gestionnaire de mots de passe compatible avec les Yubikeys. Ce projet à était conçue durant ma deuxième année de BUT Informatique par 5 membres. Cette application à était réalisée entièrement en Flutter", link: "https://github.com/PassWorldTeam/Passworld"},
  {name: "FukaFukaShita", img: "assets/pics/rdd.jpg", description: "Site de forum de rêves/cauchemar. Ce projet à était créé dans le cadre de la matière PHP symphony en 3ème année de BUT Informatique.", link: "https://github.com/Assassymfony/Fukafukashita"},
]

export const DIPLO : Diplome[] = [
  { name: "BUT Informatique - Spécialité WEB", description: "Année d'obtention : 2024", img: "assets/pics/uca.png"},
  { name: "Bac général : Maths & Physique-Chimie", description: "Diplôme obtenue sans mention en 2021.", img: "assets/pics/lq.jpg"},
  { name: "Master Cybersécurité", description: "Actuellement accepté à l'ESAIP à Angers pour mon master en Cybersécurité.", img: "assets/pics/esaip.jpeg"},
]
